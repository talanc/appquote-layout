﻿enum End {
    Front,
    Back
}

enum Side {
    Left,
    Right
}

enum ShedType {
    Shed,
    Carport
}

enum RoofType {
    Gable,
    Monopitch,
    Mansard
}

interface Dimensions {
    span: number;
    endBays: number;

    roofSpan: number;
    roofSpanOffset: number;

    height: number;
    apexHeight: number;
    leftHeight: number;
    rightHeight: number;

    getRoofFace(side: Side): RoofFace;
    getRoofShape(end?: End, start?: Point): Point[];

    building: Building;
}

class Shed {
    type: ShedType;
    roof: RoofType;

    span: number;
    endBays: number;

    get totalSpan(): number {
        return this.span + this.leftLt.span + this.rightLt.span;
    }

    get roofSpan(): number {
        switch (this.roof) {
            case RoofType.Gable:
                return this.span / 2;
            case RoofType.Monopitch:
                return this.span;
            case RoofType.Mansard:
                return this.span * 0.35;
        }
    }
    get roofSpanOffset(): number {
        if (this.roof === RoofType.Mansard) {
            return this.span * 0.15;
        }
        return 0;
    }

    length: number;
    sideBays: number;
    customSideBays: number[];

    getSideBayOffsets(): number[]{
        if (this.customSideBays) {
            var arr: number[] = [];
            var offset = 0;

            for (var i = 0; i < this.sideBays; i++) {
                arr.push(offset);
                offset += this.customSideBays[i];
            }
            arr.push(offset);
            return arr;
        }

        return getBayOffsets(this.length, this.sideBays);
    }

    height: number;
    pitch: number;
    get pitchRad(): number {
        return util.degToRad(this.pitch);
    }
    get apexHeight(): number {
        if (this.roof === RoofType.Mansard) {
            var roofShape = this.getRoofShape(End.Front);
            var i = roofShape.length / 2 - 0.5;
            return roofShape[i].y;
        }
        return this.height + this.roofSpan * Math.tan(this.pitchRad);
    }
    get leftHeight(): number {
        if (this.roof === RoofType.Monopitch) {
            return this.apexHeight;
        }
        return this.height;
    }
    get rightHeight(): number {
        return this.height;
    }

    get building(): Building {
        return Building.Main;
    }

    getRoofFace(side: Side): RoofFace {
        switch (side) {
            case Side.Left:
                return RoofFace.MainLeft;
            case Side.Right:
                return RoofFace.MainRight;
        }
    }

    getRoofShape(end?: End, start?: Point): Point[]{
        if (end === undefined) {
            end = End.Front;
        }
        if (start === undefined) {
            start = new Point(0, 0);
        }
        var shape: Point[] = [];

        var lh = start.y + this.leftHeight;
        var rh = start.y + this.rightHeight;

        if (end === End.Back) {
            var tmp = lh;
            lh = rh;
            rh = tmp;
        }

        shape.push(new Point(start.x, lh));

        switch (this.roof) {
            case RoofType.Gable:
                shape.push(new Point(start.x + this.span / 2, start.y + this.apexHeight));
                break;
            case RoofType.Monopitch:
                // do nothing
                break;
            case RoofType.Mansard:
                var x1 = this.roofSpanOffset;
                var y1 = x1 * Math.tan(util.degToRad(66));
                var x2 = this.span * 0.5;
                var y2 = this.span * 0.35 * Math.tan(util.degToRad(22));
                var kneey = start.y + this.height + y1;
                shape.push(new Point(start.x + x1, kneey));
                shape.push(new Point(start.x + this.span * 0.5, kneey + y2));
                shape.push(new Point(start.x + this.span - x1, kneey));
                break;
        }

        shape.push(new Point(start.x + this.span, rh));

        return shape;
    }

    colours: {
        roof: string;
        wall: string;
        trim: string;
        paDoor: string;
        rollerDoor: string;
        window: string;
    };

    leftLt: Leanto;
    rightLt: Leanto;

    roofObjects: RoofObject[];
    wallObjects: WallObject[];
}

class Leanto {
    side: Side;
    start: End;

    sideBays: number;

    get length(): number {
        var offsets = this.shed.getSideBayOffsets();;
        return offsets[this.bayEnd] - offsets[this.bayStart];
    }

    span: number;
    endBays: number;

    drop: number;
    pitch: number;
    get pitchRad(): number {
        return util.degToRad(this.pitch);
    }

    get roofSpan(): number {
        return this.span;
    }
    get roofSpanOffset(): number {
        return 0;
    }

    get height(): number {
        return this.apexHeight - this.span * Math.tan(this.pitchRad);
    }
    get apexHeight(): number {
        return this.mainHeight - this.drop;
    }
    get leftHeight(): number {
        if (this.side === Side.Left) {
            return this.height;
        }
        return this.apexHeight;
    }
    get rightHeight(): number {
        if (this.side === Side.Right) {
            return this.height;
        }
        return this.apexHeight;
    }

    get bayStart(): number {
        if (this.start === End.Front) {
            return 0;
        }
        return this.shed.sideBays - this.sideBays;
    }

    get bayEnd(): number {
        if (this.start === End.Back) {
            return this.shed.sideBays;
        }
        return this.sideBays + this.shed.sideBays - this.sideBays;
    }

    get lengthStart(): number {
        if (this.start === End.Front) {
            return 0;
        }
        var offsets = this.shed.getSideBayOffsets();
        return offsets[this.bayStart];
    }
    get lengthEnd(): number {
        if (this.start === End.Back) {
            return this.shed.length;
        }
        var offsets = this.shed.getSideBayOffsets();
        return offsets[this.bayEnd];
    }

    get mainHeight(): number {
        switch (this.side) {
            case Side.Left:
                return this.shed.leftHeight;
            case Side.Right:
                return this.shed.rightHeight;
        }
    }

    get building(): Building {
        switch (this.side) {
            case Side.Left:
                return Building.LeftLeanto;
            case Side.Right:
                return Building.RightLeanto;
        }
    }

    getRoofFace(side: Side): RoofFace {
        switch (this.side) {
            case Side.Left:
                return RoofFace.LeftLeanto;
            case Side.Right:
                return RoofFace.RightLeanto;
        }
    }

    getRoofShape(end?: End, start?: Point): Point[] {
        function xor(a: boolean, b: boolean) {
            return (a || b) && !(a && b);
        }
        if (end === undefined) {
            end = End.Front;
        }
        if (start === undefined) {
            start = new Point();
        }

        var left = this.height;
        var right = this.apexHeight;

        if (xor(end === End.Back, this.side === Side.Right)) {
            var tmp = left;
            left = right;
            right = tmp;
        }

        return [
            new Point(start.x, start.y + left),
            new Point(start.x + this.span, start.y + right)
        ];
    }

    private shed: Shed;

    constructor(side: Side, shed: Shed) {
        this.side = side;
        this.shed = shed;

        this.start = End.Front;
        this.sideBays = 1;
        this.span = 0;
        this.endBays = 1;
        this.drop = 0.3;
        this.pitch = 11;
    }
}

function defaultShed(): Shed {
    var shed = new Shed();
    shed.type = ShedType.Shed;
    shed.roof = RoofType.Gable;

    shed.span = 4;
    shed.endBays = 1;

    shed.length = 6;
    shed.sideBays = 2;

    shed.height = 3;
    shed.pitch = 22;

    shed.colours = {
        roof: Colours.CottageGreen,
        wall: Colours.ClassicCream,
        trim: Colours.NightSky,
        paDoor: Colours.CottageGreen,
        rollerDoor: Colours.CottageGreen,
        window: Colours.Surfmist
    };

    shed.leftLt = new Leanto(Side.Left, shed);
    shed.leftLt.start = End.Back;
    shed.rightLt = new Leanto(Side.Right, shed);
    shed.rightLt.span = 2;

    var skylight1 = new Skylight(shed);
    skylight1.face = RoofFace.MainRight;
    skylight1.bay = 2;

    var skylight2 = new Skylight(shed);
    skylight2.face = RoofFace.RightLeanto;
    skylight2.bay = 1;

    shed.roofObjects = [skylight1, skylight2];

    if (shed.roof !== RoofType.Monopitch) {
        var skylight3 = new Skylight(shed);
        skylight3.face = RoofFace.MainLeft;
        skylight3.bay = 1;
        shed.roofObjects.push(skylight3);
    }

    var paDoor1 = new PersonDoor(shed);
    paDoor1.building = Building.Main;
    paDoor1.face = WallFace.Left;
    paDoor1.bay = 1;
    paDoor1.position = WallPosition.Centre;

    var paDoor2 = new PersonDoor(shed);
    paDoor2.building = Building.Main;
    paDoor2.face = WallFace.Back;
    paDoor2.bay = 1;
    paDoor2.position = WallPosition.Right;

    var rd1 = new RollerDoor(shed);
    rd1.building = Building.Main;
    rd1.face = WallFace.Front;
    rd1.bay = 1;
    rd1.position = WallPosition.Centre;

    var win1 = new GlassWindow(shed);
    win1.building = Building.Main;
    win1.face = WallFace.Left;
    win1.bay = 2;
    win1.position = WallPosition.Left;

    var gd1 = new GlassDoor(shed);
    gd1.building = Building.Main;
    gd1.face = WallFace.Back;
    gd1.bay = 1;
    gd1.position = WallPosition.Left;

    shed.wallObjects = [paDoor1, paDoor2, rd1, win1, gd1];
    
    return shed;
}
