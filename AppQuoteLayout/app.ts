﻿
window.onload = () => {
    var canvas: HTMLElement = document.getElementById("layout_canvas");
    var warningElement = document.getElementById("layout_warnings");
    
    var renderShed = (shed: Shed) => {
        var canvas: any = document.getElementById("layout_canvas");
        var ctx = canvas.getContext("2d");
        var layout = new Layout(ctx, shed);
        layout.render();
    }

    if (window.location.search.length === 0) {
        renderShed(defaultShed());
        return;
    }
    
    var xhr = new XMLHttpRequest();

    xhr.onreadystatechange = () => {
        if (xhr.readyState === 4) {
            if (xhr.status === 200) {
                try {
                    var shedXml = util.shedFromXml(xhr.responseXML.documentElement);
                    if (shedXml.err !== null) {
                        var warnings = shedXml.err.reduce((previousValue, currentValue) => {
                            return previousValue + "<br />" + currentValue;
                        });
                        warningElement.innerHTML = "<b>Warnings:</b><br />" + warnings;
                        canvas.setAttribute("data-warning", warnings);
                    }
                    else {
                        renderShed(shedXml.shed);
                    }
                }
                catch (e) {
                    warningElement.innerHTML = "<b>Error:</b>: " + e;
                    canvas.setAttribute("data-error", e);
                }
            }
            else {
                warningElement.innerHTML = "<b>Error:</b>: There was a problem";
                canvas.setAttribute("data-error", "Unknown error");
            }

            // loaded
            canvas.setAttribute("data-loaded", "");
        }
    };

    var getUrl = config.url + window.location.search + "&xml";
    xhr.open("GET", getUrl);
    xhr.send();
};
