﻿class Layout {
    private ctx: CanvasRenderingContext2D;
    private shed: Shed;

    private scale: number;
    private layoutWidth: number;
    private layoutHeight: number;

    private MARGIN = 0.5;
    private SPACING = 1;

    private faceFront: Rectangle;
    private faceBack: Rectangle;
    private faceLeft: Rectangle;
    private faceRight: Rectangle;
    private facePlan: Rectangle;

    constructor(ctx: CanvasRenderingContext2D, shed: Shed) {
        this.ctx = ctx;
        this.shed = shed;
    }

    private calculateFacePositions() {
        var margin = this.MARGIN;
        var spacing = this.SPACING;

        var shed = this.shed;
        var totalSpan = shed.totalSpan;
        var length = shed.length;
        var apexHeight = shed.apexHeight;

        /*
        Calculates the positions on the layout to look like so:

                       [ Left   Side ]

        [ Front End ]  [ Plan / Roof ]  [ Back End ]

                       [ Right  Side ]
        */

        var x1 = margin;
        var x2 = x1 + totalSpan + spacing;
        var x3 = x2 + length + spacing;

        var y1 = margin;
        var y2 = y1 + apexHeight + spacing + shed.rightLt.span;
        var y3 = y2 + shed.span + shed.leftLt.span + spacing;

        this.faceFront = new Rectangle(x1, y2, totalSpan, apexHeight);
        this.faceBack = new Rectangle(x3, y2, totalSpan, apexHeight);
        this.faceLeft = new Rectangle(x2, y3, length, apexHeight);
        this.faceRight = new Rectangle(x2, y1, apexHeight);
        this.facePlan = new Rectangle(x2, y2, length, shed.span + shed.leftLt.span);

        this.layoutWidth = this.faceBack.x2 + this.MARGIN;
        this.layoutHeight = this.faceLeft.y2 + this.MARGIN;
    }

    private calcScale() {
        var canv = this.ctx.canvas;
        var wreq = canv.width / this.layoutWidth;
        var hreq = canv.height / this.layoutHeight;
        this.scale = Math.floor(Math.min(wreq, hreq));
    }

    private resizeCanvas() {
        var canv = this.ctx.canvas;
        var newWidth = Math.ceil(this.scale * this.layoutWidth);
        var newHeight = Math.ceil(this.scale * this.layoutHeight);
        
        canv.width = newWidth;
        canv.style.width = newWidth.toString();

        canv.height = newHeight;
        canv.style.height = newHeight.toString();
    }

    render() {
        var ctx = this.ctx;
        var canv = ctx.canvas;

        this.calculateFacePositions();
        this.calcScale();
        this.resizeCanvas();

        ctx.lineWidth = this.scale / 1000;

        ctx.save();

        // make lines look 'crisp'
        ctx.translate(0.5, 0.5);

        // set bottom left as origin point
        ctx.scale(1, -1);
        ctx.translate(0, -canv.height);

        ctx.save();
        ctx.scale(this.scale, this.scale);

        var layout: IRenderLayout;
        switch (this.shed.type) {
            case ShedType.Shed:
                layout = new ShedLayout();
                break;
            case ShedType.Carport:
                layout = new CarportLayout();
                break;
        }

        layout.init(ctx, this.scale, this.shed);

        layout.renderRoof(this.facePlan.x, this.facePlan.y);

        layout.renderEnd(End.Front, this.faceFront.x + this.shed.leftLt.span, this.faceFront.y);
        layout.renderEnd(End.Back, this.faceBack.x + this.shed.rightLt.span, this.faceBack.y);

        layout.renderSide(Side.Left, this.faceLeft.x, this.faceLeft.y);
        layout.renderSide(Side.Right, this.faceRight.x, this.faceRight.y);

        ctx.restore();

        //
        // do drawing here that doesn't rely on scaling
        //

        ctx.restore();
    }
}

interface IRenderLayout {
    init(ctx: CanvasRenderingContext2D, scale: number, shed: Shed);
    renderEnd(end: End, startx: number, starty: number);
    renderSide(side: Side, startx: number, starty: number);
    renderRoof(startx: number, starty: number);
}

class ShedLayout implements IRenderLayout {
    private ctx: CanvasRenderingContext2D;
    private scale: number;
    private shed: Shed;

    private drawLine(x1: number, y1: number, x2: number, y2: number) {
        drawLine(this.ctx, x1, y1, x2, y2);
    }

    init(ctx: CanvasRenderingContext2D, scale: number, shed: Shed) {
        this.ctx = ctx;
        this.scale = scale;
        this.shed = shed;
    }

    renderEnd(end: End, startx: number, starty: number) {
        var start = new Point(startx, starty);
        var ctx = this.ctx;
        var shed = this.shed;

        var rightGround = new Point(start.x + shed.span, start.y);

        var face = WallFace.Front;
        var llt = shed.leftLt;
        var rlt = shed.rightLt;
        if (end === End.Back) {
            face = WallFace.Back;
            llt = shed.rightLt;
            rlt = shed.leftLt;
        }

        ctx.fillStyle = shed.colours.wall;
        ctx.strokeStyle = "black";

        ctx.beginPath();
        ctx.moveTo(startx, starty);

        if (llt.span > 0) {
            ctx.lineTo(start.x - llt.span, start.y);
            ctx.lineTo(start.x - llt.span, start.y + llt.height);
            ctx.lineTo(start.x, start.y + llt.apexHeight);
        }

        var roofShape = shed.getRoofShape(end, start);
        roofShape.forEach(pt => {
            ctx.lineTo(pt.x, pt.y);
        });

        if (rlt.span > 0) {
            ctx.lineTo(rightGround.x, rightGround.y + rlt.apexHeight);
            ctx.lineTo(rightGround.x + rlt.span, rightGround.y + rlt.height);
            ctx.lineTo(rightGround.x + rlt.span, rightGround.y);
        }

        ctx.lineTo(rightGround.x, rightGround.y);

        ctx.closePath();
        ctx.fill();
        ctx.stroke();

        if (llt.span === 0 || llt.start !== end || llt.sideBays === shed.sideBays) {
            this.drawLine(start.x, start.y, start.x, start.y + llt.mainHeight);
        }

        if (rlt.span === 0 || rlt.start !== end || rlt.sideBays === shed.sideBays) {
            this.drawLine(rightGround.x, rightGround.y, rightGround.x, rightGround.y + rlt.mainHeight);
        }

        var getOpenBay = (building: Building) => {
            var openBays = shed.wallObjects.filter((wo, i, arr) => {
                return wo.building === building && wo.name === "Open Bay" && wo.face === face;
            });
            var numOpenBays = openBays.length;

            if (end === End.Front && numOpenBays === shed.sideBays) {
                return openBays[0];
            }
            if (end === End.Back && numOpenBays === 1) {
                return openBays[0];
            }

            return null;
        };

        var drawOpenBay = (building: Building, pt: Point) => {
            var openBay = getOpenBay(building);
            if (openBay) {
                openBay.render(ctx, pt);
            }
        };

        drawOpenBay(Building.Main, start);
        drawOpenBay(llt.building, start.sub2(llt.span, 0));
        drawOpenBay(rlt.building, start.add2(shed.span, 0));

        shed.wallObjects.forEach(wo => {
            if (wo.face === face && wo.name !== "Open Bay") {
                var pos = start.add(wo.getElevationPos());
                wo.render(ctx, pos);
            }
        });
    }

    renderSideBld(bld: Dimensions, side: Side, startx: number, starty: number) {
        if (bld.span === 0) {
            return;
        }

        var start = new Point(startx, starty);
        var ctx = this.ctx;

        var length = this.shed.length;

        var face = WallFace.Left;
        var building = bld.building;
        var roofFace = bld.getRoofFace(side);
        var height = bld.leftHeight;
        var roofEnd = End.Front;
        if (side === Side.Right) {
            face = WallFace.Right;
            height = bld.rightHeight;
            roofEnd = End.Back;
        }
        
        if (height < bld.apexHeight) {
            ctx.strokeStyle = "black";

            ctx.fillStyle = this.shed.colours.roof;
            ctx.fillRect(startx, starty, length, bld.apexHeight);
            ctx.strokeRect(startx, starty, length, bld.apexHeight);

            // draw roof lines
            ctx.setLineDash([this.scale / 300]);
            var roofShape = bld.getRoofShape(roofEnd, start);
            for (var i = 1; i < roofShape.length / 2 - 1; i++) {
                this.drawLine(startx, roofShape[i].y, startx + length, roofShape[i].y);
            }
            ctx.setLineDash([]);

            ctx.fillStyle = this.shed.colours.wall;
            ctx.fillRect(startx, starty, length, bld.height);
            ctx.strokeRect(startx, starty, length, bld.height);

            this.shed.roofObjects.forEach(ro => {
                if (ro.face === roofFace) {
                    var pos = start.add(ro.getElevationPos());
                    ro.renderElevation(ctx, pos);
                }
            });
        }
        else {
            ctx.fillStyle = this.shed.colours.wall;
            ctx.strokeStyle = "black";

            ctx.fillRect(startx, starty, length, bld.apexHeight);
            ctx.strokeRect(startx, starty, length, bld.apexHeight);
        }

        this.shed.wallObjects.forEach(wo => {
            if (wo.building === building && wo.face === face) {
                var pos = start.add(wo.getElevationPos());
                wo.render(ctx, pos);
            }
        });
    }

    renderSide(side: Side, startx: number, starty: number) {
        this.renderSideBld(this.shed, side, startx, starty);
        if (side === Side.Left) {
            this.renderSideBld(this.shed.leftLt, side, startx, starty);
        }
        else {
            this.renderSideBld(this.shed.rightLt, side, startx, starty);
        }
    }

    renderRoof(startx: number, starty: number) {
        var start = new Point(startx, starty);
        var ctx = this.ctx;
        var shed = this.shed;

        ctx.fillStyle = shed.colours.roof;
        ctx.strokeStyle = "black";

        ctx.fillRect(startx, starty, shed.length, shed.span);
        ctx.strokeRect(startx, starty, shed.length, shed.span);

        this.renderRoofLeanto(Side.Left, shed.leftLt, new Point(startx, starty + shed.span));
        this.renderRoofLeanto(Side.Right, shed.rightLt, start);

        shed.roofObjects.forEach(ro => {
            var pos = start.add(ro.getPlanPos());
            ro.render(ctx, pos);
        });

        ctx.strokeStyle = "black";
        ctx.setLineDash([this.scale / 300]);

        var roofShape = shed.getRoofShape(End.Front);
        for (var i = 1; i < roofShape.length - 1; i++) {
            var y = start.y + roofShape[i].x;
            this.drawLine(start.x, y, start.x + shed.length, y);
        }

        ctx.setLineDash([]);

        if (shed.leftLt.span > 0 && shed.leftLt.drop > 0) {
            var y = start.y + shed.span;
            this.drawLine(start.x, y, start.x + shed.length, y);
        }

        if (shed.rightLt.span > 0 && shed.rightLt.drop > 0) {
            var y = start.y;
            this.drawLine(start.x, y, start.x + shed.length, y);
        }
    }

    private renderRoofLeanto(side: Side, lt: Leanto, start: Point) {
        if (lt.span === 0) {
            return;
        }

        var ctx = this.ctx;
        var shed = this.shed;

        var mod = 1;
        if (side === Side.Right) {
            mod = -1;
        }

        var offsets = this.shed.getSideBayOffsets();

        var i = 0;
        if (lt.start === End.Back) {
            i = shed.sideBays - lt.sideBays;
        }

        var x = start.x + offsets[i];
        var y = start.y;
        var w = offsets[i + lt.sideBays] - offsets[i];
        var h = lt.span * mod;

        ctx.fillStyle = shed.colours.roof;
        ctx.strokeStyle = "black";
        ctx.fillRect(x, y, w, h);
        ctx.strokeRect(x, y, w, h);
    }
}

class CarportLayout implements IRenderLayout {
    private ctx: CanvasRenderingContext2D;
    private scale: number;
    private shed: Shed;

    private drawLine(x1: number, y1: number, x2: number, y2: number) {
        drawLine(this.ctx, x1, y1, x2, y2);
    }

    init(ctx: CanvasRenderingContext2D, scale: number, shed: Shed) {
        this.ctx = ctx;
        this.scale = scale;
        this.shed = shed;
    }

    renderEnd(end: End, startx: number, starty: number) {
        var start = new Point(startx, starty);
        var ctx = this.ctx;
        var shed = this.shed;

        var roofShape = shed.getRoofShape(end, start);

        ctx.strokeStyle = "black";

        ctx.beginPath();
        ctx.moveTo(startx, starty);
        roofShape.forEach(pt => {
            ctx.lineTo(pt.x, pt.y);
        });
        ctx.lineTo(startx + shed.span, starty);
        ctx.stroke();
    }

    renderSide(side: Side, startx: number, starty: number) {
        var start = new Point(startx, starty);
        var ctx = this.ctx;
        var shed = this.shed;

        var height = shed.height;
        var apexHeight = shed.apexHeight;

        var showExtRoof = (shed.leftHeight < apexHeight);
        if (side === Side.Right) {
            showExtRoof = (shed.rightHeight < apexHeight);
        }

        if (showExtRoof) {
            ctx.fillStyle = shed.colours.roof;
        }
        else {
            ctx.fillStyle = "lightgray";
        }
        ctx.strokeStyle = "black";

        ctx.fillRect(startx, starty + height, shed.length, apexHeight - height);
        
        shed.roofObjects.forEach(ro => {
            var pos = start.add(ro.getElevationPos());
            ro.renderElevation(ctx, pos);
        });

        ctx.strokeRect(startx, starty + height, shed.length, apexHeight - height);

        var postHeight = height;
        if (!showExtRoof) {
            postHeight = apexHeight;
        }
        var offsets = shed.getSideBayOffsets();
        offsets.forEach(offset => {
            var pt = start.add2(offset, 0);
            this.drawLine(pt.x, pt.y, pt.x, pt.y + postHeight);
        });
    }

    renderRoof(startx: number, starty: number) {
        var start = new Point(startx, starty);
        var ctx = this.ctx;
        var shed = this.shed;

        ctx.fillStyle = shed.colours.roof;
        ctx.strokeStyle = "black";

        ctx.fillRect(startx, starty, shed.length, shed.span);

        shed.roofObjects.forEach(ro => {
            var pos = start.add(ro.getPlanPos());
            ro.render(ctx, pos);
        });

        ctx.strokeRect(startx, starty, shed.length, shed.span);

        ctx.strokeStyle = "black";
        ctx.setLineDash([this.scale / 300]);

        var roofShape = shed.getRoofShape(End.Front);
        for (var i = 1; i < roofShape.length - 1; i++) {
            var y = start.y + roofShape[i].x;
            this.drawLine(start.x, y, start.x + shed.length, y);
        }

        ctx.setLineDash([]);

        if (shed.leftLt.span > 0 && shed.leftLt.drop > 0) {
            var y = start.y + shed.span;
            this.drawLine(start.x, y, start.x + shed.length, y);
        }

        if (shed.rightLt.span > 0 && shed.rightLt.drop > 0) {
            var y = start.y;
            this.drawLine(start.x, y, start.x + shed.length, y);
        }
    }
}
