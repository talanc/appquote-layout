﻿enum Building {
    Main,
    LeftLeanto,
    RightLeanto
}

enum WallFace {
    Front,
    Back,
    Left,
    Right
}

enum RoofFace {
    MainLeft,
    MainRight,
    LeftLeanto,
    RightLeanto
}

enum WallPosition {
    None,
    Left,
    Centre,
    Right
}

function InvWallFace(face: WallFace): WallFace {
    switch (face) {
        case WallFace.Front:
            return WallFace.Back;
        case WallFace.Back:
            return WallFace.Front;
        case WallFace.Left:
            return WallFace.Right;
        case WallFace.Right:
            return WallFace.Left;
    }
}

function InvWallPosition(pos: WallPosition): WallPosition {
    switch (pos) {
        case WallPosition.None:
            return WallPosition.None;
        case WallPosition.Centre:
            return WallPosition.Centre;
        case WallPosition.Left:
            return WallPosition.Right;
        case WallPosition.Right:
            return WallPosition.Left;
    }
}

class WallObject {
    name: string;
    building: Building;
    face: WallFace;
    bay: number;
    position: WallPosition;

    shed: Shed;

    width: number;
    height: number;

    offsetX = 0.2;

    constructor(shed: Shed, name: string) {
        this.shed = shed;
        this.name = name;
    }

    getBuilding(): Dimensions {
        switch (this.building) {
            case Building.Main:
                return this.shed;
            case Building.LeftLeanto:
                return this.shed.leftLt;
            case Building.RightLeanto:
                return this.shed.rightLt;
        }
    }

    getOffsets(): number[] {
        switch (this.face) {
            case WallFace.Front:
            case WallFace.Back:
                var dim = this.getBuilding();
                return getBayOffsets(dim.span, dim.endBays);
            case WallFace.Left:
            case WallFace.Right:
                return this.shed.getSideBayOffsets();;
        }
    }

    getElevationPos(): Point {
        var pos = new Point();

        if ((this.face === WallFace.Front || this.face === WallFace.Back) && this.bay === 0) {
            var building = this.getBuilding();
            pos.x = building.span / 2 - this.width / 2;
            pos.y = building.apexHeight - 0.5 - this.height;
            return pos;
        }

        switch (this.building) {
            case Building.Main:
                pos.x = 0;
                break;

            case Building.LeftLeanto:
                switch (this.face) {
                    case WallFace.Front:
                        pos.x = -this.shed.leftLt.span;
                        break;
                    case WallFace.Back:
                        pos.x = this.shed.span;
                        break;
                }
                break;

            case Building.RightLeanto:
                switch (this.face) {
                    case WallFace.Front:
                        pos.x = this.shed.span;
                        break;
                    case WallFace.Back:
                        pos.x = -this.shed.rightLt.span;
                        break;
                }
                break;
        }

        var offsets = this.getOffsets();

        // reverse position
        var position = this.position;
        if (this.face === WallFace.Left) {
            position = InvWallPosition(position);
        }

        // reverse bay
        var bay = this.bay;
        if (this.face === WallFace.Back) {
            bay = offsets.length - this.bay;
        }
        
        var l = offsets[bay - 1];
        var r = offsets[bay];

        switch (position) {
            case WallPosition.None:
                pos.x += l;
                break;
            case WallPosition.Left:
                pos.x += l + this.offsetX;
                break;
            case WallPosition.Right:
                pos.x += r - this.width - this.offsetX;
                break;
            case WallPosition.Centre:
                pos.x += l + (r - l) / 2 - this.width / 2
                break;
        }

        pos.y = 0;

        return pos;
    }

    render(ctx: CanvasRenderingContext2D, pos: Point) {
    }
}

class PersonDoor extends WallObject {

    constructor(shed: Shed) {
        super(shed, "PA Door");

        this.width = 0.9;
        this.height = 2.4;
    }

    render(ctx: CanvasRenderingContext2D, pos: Point) {
        ctx.fillStyle = this.shed.colours.paDoor;
        ctx.fillRect(pos.x, pos.y, this.width, this.height);

        ctx.strokeStyle = "black";
        ctx.strokeRect(pos.x, pos.y, this.width, this.height);

        // door knob
        var x = pos.x + this.width - 0.2;
        var y = pos.y + 1.1;
        ctx.beginPath();
        ctx.arc(x, y, 0.06, 0, Math.PI * 2);
        ctx.stroke();
    }
}

class RollerDoor extends WallObject {
    constructor(shed: Shed) {
        super(shed, "Roller Door");

        this.width = 2.7;
        this.height = 2.7;
    }

    render(ctx: CanvasRenderingContext2D, pos: Point) {
        ctx.fillStyle = this.shed.colours.rollerDoor;
        ctx.fillRect(pos.x, pos.y, this.width, this.height);

        ctx.strokeStyle = "black";
        ctx.strokeRect(pos.x, pos.y, this.width, this.height);

        var hw = 0.5;
        var hh = 0.1;
        var hx = pos.x + this.width / 2 - hw / 2;
        var hy = pos.y + this.height * 0.35;
        ctx.strokeRect(hx, hy, hw, hh);
    }
}

// 'Window' already taken :(
class GlassWindow extends WallObject {
    offsetY: number = 1.3;

    constructor(shed: Shed) {
        super(shed, "Window");

        this.width = 1.2;
        this.height = 0.8;
    }

    getElevationPos(): Point {
        return super.getElevationPos().add(new Point(0, this.offsetY));
    }

    render(ctx: CanvasRenderingContext2D, pos: Point) {
        ctx.fillStyle = "ivory";
        ctx.fillRect(pos.x, pos.y, this.width, this.height);

        ctx.strokeStyle = this.shed.colours.window;
        ctx.strokeRect(pos.x, pos.y, this.width, this.height);

        var x = pos.x + this.width / 2;
        ctx.beginPath();
        ctx.moveTo(x, pos.y);
        ctx.lineTo(x, pos.y + this.height);
        ctx.stroke();
    }
}

class BarnWindow extends WallObject {
    constructor(shed: Shed) {
        super(shed, "Barn Window");

        this.width = 0.840;
        this.height = 0.475;
    }

    render(ctx: CanvasRenderingContext2D, pos: Point) {

        var hm = this.height - (this.width / 2 * Math.tan(util.degToRad(22)));
        var p1 = pos;
        var p2 = p1.add2(0, hm);
        var p3 = p1.add2(this.width / 2, this.height);
        var p4 = p2.add2(this.width, 0);
        var p5 = p1.add2(this.width, 0);

        ctx.fillStyle = "ivory";
        ctx.strokeStyle = this.shed.colours.window;

        ctx.beginPath();
        ctx.moveTo(p1.x, p1.y);
        ctx.lineTo(p2.x, p2.y);
        ctx.lineTo(p3.x, p3.y);
        ctx.lineTo(p4.x, p4.y);
        ctx.lineTo(p5.x, p5.y);
        ctx.closePath();
        ctx.fill();
        ctx.stroke();

        var mid = p1.add2(this.width / 2, 0);
        drawLinePt(ctx, p2, mid);
        drawLinePt(ctx, p3, mid);
        drawLinePt(ctx, p4, mid);
    }
}

class GlassDoor extends WallObject {
    constructor(shed: Shed) {
        super(shed, "Glass Door");

        this.width = 1.8;
        this.height = 2.1;
    }

    render(ctx: CanvasRenderingContext2D, pos: Point) {
        var y = pos.y + 0.05;
        
        ctx.fillStyle = "ivory";
        ctx.fillRect(pos.x, y, this.width, this.height);

        ctx.strokeStyle = this.shed.colours.window;
        ctx.strokeRect(pos.x, y, this.width, this.height);

        var x = pos.x + this.width / 2;
        ctx.beginPath();
        ctx.moveTo(x, y);
        ctx.lineTo(x, y + this.height);
        ctx.stroke();
    }
}

class IndustrialDoor extends WallObject {
    constructor(shed: Shed) {
        super(shed, "Industrial Door");

        this.width = 1.8;
        this.height = 2.1;
    }

    render(ctx: CanvasRenderingContext2D, pos: Point) {
        ctx.strokeRect(pos.x, pos.y, this.width, this.height);
    }
}

class OpenBay extends WallObject {
    constructor(shed: Shed) {
        super(shed, "Open Bay");
    }

    getElevationPos(): Point {
        var dim = super.getBuilding();

        switch (this.face) {
            case WallFace.Front:
            case WallFace.Back:
                this.width = dim.span / dim.endBays;
                break;
            case WallFace.Left:
            case WallFace.Right:
                var offsets = this.getOffsets();
                this.width = offsets[this.bay] - offsets[this.bay - 1];
                break;
        }

        this.height = dim.height;

        return super.getElevationPos();
    }

    render(ctx: CanvasRenderingContext2D, pos: Point) {
        ctx.fillStyle = "#99CCFF";
        ctx.strokeStyle = "black";

        switch (this.face) {
            case WallFace.Front:
            case WallFace.Back:
                this.renderEnd(ctx, pos);
                break;
            case WallFace.Left:
            case WallFace.Right:
                this.renderSide(ctx, pos);
                break;
        }
    }

    renderSide(ctx: CanvasRenderingContext2D, pos: Point) {
        ctx.fillRect(pos.x, pos.y, this.width, this.height);
        ctx.strokeRect(pos.x, pos.y, this.width, this.height);
    }

    renderEnd(ctx: CanvasRenderingContext2D, pos: Point) {
        var end = End.Front;
        if (this.face === WallFace.Back) {
            end = End.Back;
        }

        var roofShape = this.getBuilding().getRoofShape(end, pos);

        ctx.beginPath();
        ctx.moveTo(pos.x, pos.y);

        roofShape.forEach(pt => {
            ctx.lineTo(pt.x, pt.y);
        });

        var lastPt = roofShape[roofShape.length - 1];
        ctx.lineTo(lastPt.x, pos.y);

        ctx.closePath();

        ctx.fill();
        ctx.stroke();
    }
}

class RoofObject {
    name: string;
    face: RoofFace;
    bay: number;

    public shed: Shed;

    constructor(shed: Shed, name: string) {
        this.shed = shed;
        this.name = name;
    }

    getBuilding(): Dimensions {
        switch (this.face) {
            case RoofFace.MainLeft:
            case RoofFace.MainRight:
                return this.shed;
            case RoofFace.LeftLeanto:
                return this.shed.leftLt;
            case RoofFace.RightLeanto:
                return this.shed.rightLt;
        }
    }

    getPlanPos(): Point {
        var shed = this.shed;
        var pos = new Point();
        var offset = this.shed.getSideBayOffsets();
        var i = this.bay - 1;

        pos.x = offset[i] + (offset[i + 1] - offset[i]) / 2;

        switch (this.face) {
            case RoofFace.MainLeft:
                pos.y = shed.span / 2;
                break;
            case RoofFace.MainRight:
                pos.y = shed.roofSpanOffset;
                break;
            case RoofFace.LeftLeanto:
                pos.y = shed.span;
                break;
            case RoofFace.RightLeanto:
                pos.y = -shed.rightLt.span;
                break;
        }
        return pos;
    }

    getElevationPos(): Point {
        var dim = this.getBuilding();
        var pos = this.getPlanPos();
        

        var roofShape = dim.getRoofShape();
        if (roofShape.length > 2) {
            var i = Math.floor(roofShape.length / 2);
            pos.y = roofShape[i - 1].y;
        }
        else {
            // TODO work out a better way of handling this
            pos.y = dim.height;
        }

        return pos;
    }

    render(ctx: CanvasRenderingContext2D, pos: Point) {
    }

    renderElevation(ctx: CanvasRenderingContext2D, pos: Point) {
    }
}

class Skylight extends RoofObject {
    width: number = 0.768;
    color: string = "lightblue";

    constructor(shed: Shed) {
        super(shed, "Skylight");
    }

    getPlanPos(): Point {
        return super.getPlanPos().sub(new Point(this.width / 2, 0));
    }

    render(ctx: CanvasRenderingContext2D, pos: Point) {
        var h = super.getBuilding().roofSpan;

        ctx.fillStyle = this.color;
        ctx.fillRect(pos.x, pos.y, this.width, h);
    }

    renderElevation(ctx: CanvasRenderingContext2D, pos: Point) {
        var shed = this.shed;


        var dim = super.getBuilding();
        var h: number;

        var roofShape = dim.getRoofShape();
        if (roofShape.length > 2) {
            var i = Math.floor(roofShape.length / 2);
            h = roofShape[i].y - roofShape[i - 1].y;
        }
        else {
            // TODO work out a better way of doing this
            h = dim.apexHeight - dim.height;
        }

        ctx.fillStyle = this.color;
        ctx.fillRect(pos.x, pos.y, this.width, h);
    }
}
