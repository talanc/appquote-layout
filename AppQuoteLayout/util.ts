﻿module util {

    export function degToRad(deg: number): number {
        return deg / 180 * Math.PI;
    }
    
    function getValue(val: string): any {
        // boolean
        if (val === "true") {
            return true;
        }
        if (val === "false") {
            return false;
        }

        // number
        // https://developer.mozilla.org/en/docs/Web/JavaScript/Reference/Global_Objects/parseFloat
        var filterFloat = function (value: string) {
            if (/^(\-|\+)?([0-9]+(\.[0-9]+)?)$/.test(value)) {
                return Number(value);
            }
            return NaN;
        }
        var num = filterFloat(val);
        if (!isNaN(num)) {
            return num;
        }

        // date
        var dt = new Date(val);
        if (!isNaN(dt.getTime())) {
            return dt;
        }

        // string... hopefully
        return val;
    }

    function getArr(xml: Element): Array<any> {
        var arr = new Array();

        var elem = xml.firstElementChild;
        while (elem !== null) {
            if (elem.childElementCount > 0) {
                arr.push(generateObjectFromXml(elem));
            } else {
                arr.push(getValue(elem.textContent));
            }
            elem = elem.nextElementSibling;
        }

        return arr;
    }

    function isArr(xml: Element): boolean {
        switch (xml.nodeName) {
            case "CustomSideBays":
            case "RoofObjects":
            case "WallObjects":
            case "Notes":
                return true;
        }

        return false;
    }

    function generateObjectFromXml(xml: Element): Object {
        if (isArr(xml)) {
            return getArr(xml);
        }

        var obj = new Object();

        var elem = xml.firstElementChild;
        while (elem !== null) {
            if (elem.childElementCount > 0) {
                obj[elem.localName] = generateObjectFromXml(elem);
            } else {
                obj[elem.localName] = getValue(elem.textContent);
            }
            elem = elem.nextElementSibling;
        }

        return obj;
    }

    function setLeanto(lt: Leanto, obj: Object) {
        if (obj === undefined) {
            return;
        }

        lt.span = obj["Span"];
        lt.drop = obj["Drop"];
        lt.pitch = obj["Pitch"];
        lt.endBays = obj["EndBays"];

        switch (obj["Start"]) {
            case "Front":
                lt.start = End.Front;
                break;
            case "Back":
                lt.start = End.Back;
                break;
        }

        lt.sideBays = obj["SideBays"];
    }

    export function shedFromXml(xml: HTMLElement): { shed: Shed; err: string[]; } {
        var errors: string[] = [];
        var obj = generateObjectFromXml(xml);

        var shed = new Shed();

        switch (obj["DesignType"]) {
            case "Shed":
                shed.type = ShedType.Shed;
                break;
            case "Carport":
                shed.type = ShedType.Carport;
                break;
            default:
                throw "Invalid DesignType: " + obj["DesignType"];
        }

        switch (obj["RoofType"]) {
            case "Gable":
                shed.roof = RoofType.Gable;
                break;
            case "Monopitch":
                shed.roof = RoofType.Monopitch;
                break;
            case "QuakerBarn":
                shed.roof = RoofType.Mansard;
                break;
            default:
                errors.push("RoofType is " + obj["RoofType"]);
        }

        shed.span = obj["Span"];
        shed.endBays = obj["EndBays"];

        shed.length = obj["Length"];
        shed.sideBays = obj["SideBays"];

        if (obj["CustomSideBays"]) {
            shed.customSideBays = [];

            obj["CustomSideBays"].forEach(bay => {
                shed.customSideBays.push(bay);
            });
        }

        shed.height = obj["Height"];
        shed.pitch = obj["Pitch"];

        var xcols = obj["Colours"];

        shed.colours = {
            roof: Colours[xcols["Roof"]],
            wall: Colours[xcols["Wall"]],
            trim: Colours[xcols["Trim"]],
            paDoor: Colours[xcols["PersonDoor"]],
            rollerDoor: Colours[xcols["RollerDoor"]],
            window: Colours[xcols["Window"]]
        };

        shed.leftLt = new Leanto(Side.Left, shed);
        setLeanto(shed.leftLt, obj["LeftLeanto"]);

        shed.rightLt = new Leanto(Side.Right, shed);
        setLeanto(shed.rightLt, obj["RightLeanto"]);

        shed.roofObjects = [];
        if (obj["RoofObjects"] === undefined) {
            obj["RoofObjects"] = [];
        }

        obj["RoofObjects"].forEach(xro => {
            var ro: RoofObject;

            switch (xro["Type"]) {
                case "Skylight":
                    ro = new Skylight(shed);
                    break;

                default:
                    ro = new RoofObject(shed, "Unknown");
                    errors.push("RoofObject Type is " + xro["Type"]);
            };

            switch (xro["Building"]) {
                case "Main":
                    switch (xro["Face"]) {
                        case "Left":
                            ro.face = RoofFace.MainLeft;
                            break;
                        case "Right":
                            ro.face = RoofFace.MainRight;
                            break;
                        default:
                            errors.push("RoofObject Building:Main Face is " + xro["Face"]);
                    }
                    break;

                case "LeftLeanto":
                    if (shed.leftLt.span === 0) {
                        errors.push("RoofObject Building:LeftLeanto building doesn't exist");
                        return;
                    }

                    if (xro["Face"] === "Left") {
                        ro.face = RoofFace.LeftLeanto;
                    }
                    else {
                        errors.push("RoofObject Building:LeftLeanto Face is " + xro["Face"]);
                    }
                    break;

                case "RightLeanto":
                    if (shed.rightLt.span === 0) {
                        errors.push("RoofObject Building:RightLeanto building doesn't exist");
                        return;
                    }

                    if (xro["Face"] === "Right") {
                        ro.face = RoofFace.RightLeanto;
                    }
                    else {
                        errors.push("RoofObject Building:RightLeanto Face is " + xro["Face"]);
                    }
                    break;

                default:
                    errors.push("RoofObject Building is " + xro["Building"]);
            }

            ro.bay = xro["Bay"];

            shed.roofObjects.push(ro);
        });

        shed.wallObjects = [];
        if (obj["WallObjects"] === undefined) {
            obj["WallObjects"] = [];
        }
        obj["WallObjects"].forEach(xwo => {
            var wo: WallObject;
            var name: string = xwo["Type"];

            switch (name) {
                case "PersonDoor":
                    var door = new PersonDoor(shed);
                    wo = door;
                    break;

                case "RollerDoor":
                    var door = new RollerDoor(shed);
                    var data = xwo["Data"].split(" x ");
                    door.height = data[0];
                    door.width = data[1];
                    wo = door;
                    break;

                case "Window":
                    var window = new GlassWindow(shed);
                    var data = xwo["Data"].split(" x ");
                    window.height = parseInt(data[0]) / 1000;
                    window.width = parseInt(data[1]) / 1000;
                    wo = window;
                    break;

                case "BarnWindow":
                    var barnWindow = new BarnWindow(shed);
                    wo = barnWindow;
                    break;

                case "GlassDoor":
                    var door = new GlassDoor(shed);
                    wo = door;
                    break;

                case "SlidingDoor":
                    var door = new IndustrialDoor(shed);
                    wo = door;
                    break;

                case "OpenBay":
                    var openBay = new OpenBay(shed);
                    wo = openBay;
                    break;

                default:
                    name = "WallObject"
                    wo = new WallObject(shed, name);
                    errors.push("WallObject Type is " + xwo["Type"]);
            }

            switch (xwo["Building"]) {
                case "Main":
                    wo.building = Building.Main;
                    break;
                case "LeftLeanto":
                    if (shed.leftLt.span === 0) {
                        errors.push(name + " Building:LeftLeanto building does not exist");
                        return;
                    }
                    wo.building = Building.LeftLeanto;
                    break;
                case "RightLeanto":
                    if (shed.rightLt.span === 0) {
                        errors.push(name + " Building:LeftLeanto building does not exist");
                        return;
                    }
                    wo.building = Building.RightLeanto;
                    break;
                default:
                    errors.push(name + " Building is " + xwo["Building"]);
            }

            switch (xwo["Face"]) {
                case "Front":
                    wo.face = WallFace.Front;
                    break;
                case "Back":
                    wo.face = WallFace.Back;
                    break;
                case "Left":
                    wo.face = WallFace.Left;
                    break;
                case "Right":
                    wo.face = WallFace.Right;
                    break;
                default:
                    errors.push(name + " Face is " + xwo["Face"]);
            }

            wo.bay = xwo["Bay"];

            switch (xwo["Side"]) {
                case "None":
                    wo.position = WallPosition.None;
                    break;
                case "Centre":
                    wo.position = WallPosition.Centre;
                    break;
                case "Left":
                    wo.position = WallPosition.Left;
                    break;
                case "Right":
                    wo.position = WallPosition.Right;
                    break;
                default:
                    errors.push(name + " Side is " + xwo["Side"]);
            }

            shed.wallObjects.push(wo);
        });

        if (errors.length === 0) {
            errors = null;
        }

        return {
            shed: shed,
            err: errors
        };
    }
}

function getBayOffsets(total: number, bays: number): number[] {
    var arr: number[] = [];
    var naiveWidth = parseFloat((total / bays).toFixed(3));
    var offset = 0;

    for (var i = 0; i < bays; i++) {
        arr.push(offset);
        offset += naiveWidth;
    }
    arr.push(total);

    return arr;
}

function drawLine(ctx: CanvasRenderingContext2D, x1: number, y1: number, x2: number, y2: number) {
    ctx.beginPath();
    ctx.moveTo(x1, y1);
    ctx.lineTo(x2, y2);
    ctx.stroke();
}

function drawLinePt(ctx: CanvasRenderingContext2D, p1: Point, p2: Point) {
    ctx.beginPath();
    ctx.moveTo(p1.x, p1.y);
    ctx.lineTo(p2.x, p2.y);
    ctx.stroke();
}


class Point {
    x: number;
    y: number;

    constructor(x: number = 0, y: number = 0) {
        this.x = x;
        this.y = y;
    }

    neg(): Point {
        return new Point(-this.x, -this.y);
    }
    add(pt: Point): Point {
        return new Point(this.x + pt.x, this.y + pt.y);
    }
    add2(x: number, y: number) {
        return new Point(this.x + x, this.y + y);
    }
    sub(pt: Point): Point {
        return new Point(this.x - pt.x, this.y - pt.y);
    }
    sub2(x: number, y: number) {
        return new Point(this.x - x, this.y - y);
    }
}

class Rectangle {
    x: number;
    y: number;
    w: number;
    h: number;

    constructor(x = 0, y = 0, w = 0, h = 0) {
        this.x = x;
        this.y = y;
        this.w = w;
        this.h = h;
    }

    get x2(): number {
        return this.x + this.w;
    }
    get y2(): number {
        return this.y + this.h;
    }
}
