﻿module Colours {
    // AU + NZ
    export var Zincalume = "#999999";

    // AU
    export var Basalt = "#6D6C6E";
    export var Bushland = "#8A8A7F";
    export var ClassicCream = "#E9DCB8";
    export var CottageGreen = "#304C3C";
    export var Cove = "#A59F8A";
    export var DeepOcean = "#364152";
    export var Dune = "#B1ADA3";
    export var EveningHaze = "#C5C2AA";
    export var Gully = "#857E73";
    export var Headland = "#975540";
    export var Ironstone = "#3E434C";
    export var Jasper = "#6C6153";
    export var Loft = "#44393D";
    export var Mangrove = "#737562";
    export var ManorRed = "#5E1D0E";
    export var Monument = "#323233";
    export var NightSky = "#000000";
    export var PaleEucalypt = "#7C846A";
    export var Paperbark = "#CABFA4";
    export var Sandbank = "#D1B988";
    export var ShaleGrey = "#BDBFBA";
    export var Surfmist = "#E4E2D5";
    export var Terrain = "#67432E";
    export var Wallaby = "#7F7C78";
    export var Wilderness = "#65796D";
    export var Windspray = "#888B8A";
    export var WoodlandGrey = "#4B4C46";

    // NZ
    export var Azure = "rgb(77,109,125)";
    export var BoneWhite = "rgb(182,182,168)";
    export var Cloud = "rgb(218,217,204)";
    export var DesertSand = "rgb(184,168,144)";
    export var Ebony = "rgb(43,43,44)";
    export var GreyFriars = "rgb(71,74,77)";
    export var GullGrey = "rgb(172,176,173)";
    export var Ironsand = "rgb(65,63,61)";
    export var Karaka = "rgb(59,61,54)";
    export var Lichen = "rgb(136,128,107)";
    export var Lignite = "rgb(77,69,64)";
    export var MistGreen = "rgb(116,126,106)";
    export var NewDenimBlue = "rgb(72,78,87)";
    export var PacificBlue = "rgb(72,91,104)";
    export var PermanentGreen = "rgb(55,80,70)";
    export var PioneerRed = "rgb(129,63,57)";
    export var Rivergum = "rgb(216,202,184)";
    export var SandstoneGrey = "rgb(125,125,123)";
    export var Scoria = "rgb(103,59,54)";
    export var SmoothCream = "rgb(229,209,170)";
    export var Stone = "rgb(152,137,121)";
    export var StormBlue = "rgb(62,73,84)";
    export var Terracotta = "rgb(164,102,75)";
    export var Titania = "rgb(215,212,200)"
}
